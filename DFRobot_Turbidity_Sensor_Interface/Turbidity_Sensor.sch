EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5200 4700 5200 4600
Wire Wire Line
	5500 4600 5500 4700
Wire Wire Line
	5500 4700 5200 4700
Connection ~ 5200 4700
Text Label 5200 4700 0    10   ~ 0
GND
Wire Wire Line
	5200 3700 5200 3800
Text Label 5200 3700 0    10   ~ 0
GND
Wire Wire Line
	3100 4200 3400 4200
Wire Wire Line
	3100 4200 2700 4200
Connection ~ 3100 4200
Text Label 3100 4200 0    10   ~ 0
GND
Wire Wire Line
	7700 4700 7700 4800
Wire Wire Line
	7700 4800 8100 4800
Wire Wire Line
	8100 4800 8100 4700
Connection ~ 8100 4800
Text Label 7700 4700 0    10   ~ 0
GND
Wire Wire Line
	8300 4000 8300 3800
Text Label 8400 4000 0    10   ~ 0
+5V
Wire Wire Line
	4400 4100 4700 4100
Wire Wire Line
	4700 4100 4700 3400
Wire Wire Line
	4700 3400 5200 3400
Connection ~ 4700 4100
Connection ~ 4700 3400
Text Label 4400 4100 0    10   ~ 0
+5V
Wire Wire Line
	8400 4100 7700 4100
Wire Wire Line
	7700 4100 7700 4300
Wire Wire Line
	5600 4200 5500 4200
Wire Wire Line
	5500 4200 5200 4200
Wire Wire Line
	5200 4200 4700 4200
Wire Wire Line
	4700 4200 4400 4200
Wire Wire Line
	5200 4300 5200 4200
Connection ~ 4700 4200
Connection ~ 5200 4200
Connection ~ 5500 4200
Wire Wire Line
	4700 4300 4400 4300
Wire Wire Line
	4400 4400 4700 4400
Wire Wire Line
	4700 4500 4400 4500
Wire Wire Line
	3400 4500 3100 4500
Wire Wire Line
	3100 4400 3400 4400
Wire Wire Line
	3400 4300 3100 4300
Wire Wire Line
	3400 4100 3100 4100
Wire Wire Line
	8100 4300 8100 4200
Wire Wire Line
	8100 4200 8400 4200
$Comp
L Turbidity_Sensor-rescue:ADAFRUIT_TRINKET-Turbidity_Sensor-eagle-import U$1
U 1 1 4D826FCC
P 3600 4000
F 0 "U$1" H 3600 4000 59  0000 L BNN
F 1 "ADAFRUIT_TRINKET" H 3600 4000 50  0001 C CNN
F 2 "Turbidity_Sensor:ADAFRUIT_TRINKET" H 3600 4000 50  0001 C CNN
F 3 "" H 3600 4000 50  0001 C CNN
	1    3600 4000
	1    0    0    -1  
$EndComp
$Comp
L Turbidity_Sensor-rescue:R-US_R0805-Turbidity_Sensor-eagle-import R1
U 1 1 85A54EB2
P 8100 4500
F 0 "R1" H 7950 4559 59  0000 L BNN
F 1 "500k" H 7950 4370 59  0000 L BNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 8100 4500 50  0001 C CNN
F 3 "" H 8100 4500 50  0001 C CNN
	1    8100 4500
	0    -1   -1   0   
$EndComp
$Comp
L Turbidity_Sensor-rescue:R-US_R0805-Turbidity_Sensor-eagle-import R2
U 1 1 8E8A2486
P 7700 4500
F 0 "R2" H 7550 4559 59  0000 L BNN
F 1 "4k7" H 7550 4370 59  0000 L BNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 7700 4500 50  0001 C CNN
F 3 "" H 7700 4500 50  0001 C CNN
	1    7700 4500
	0    -1   -1   0   
$EndComp
$Comp
L Turbidity_Sensor-rescue:R-US_R0805-Turbidity_Sensor-eagle-import R3
U 1 1 33D130BE
P 7500 4100
F 0 "R3" H 7350 4159 59  0000 L BNN
F 1 "10k" H 7350 3970 59  0000 L BNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 7500 4100 50  0001 C CNN
F 3 "" H 7500 4100 50  0001 C CNN
	1    7500 4100
	1    0    0    -1  
$EndComp
$Comp
L Turbidity_Sensor-rescue:R-US_R0805-Turbidity_Sensor-eagle-import R4
U 1 1 304D48AD
P 5500 4400
F 0 "R4" H 5350 4459 59  0000 L BNN
F 1 "10k" H 5350 4270 59  0000 L BNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5500 4400 50  0001 C CNN
F 3 "" H 5500 4400 50  0001 C CNN
	1    5500 4400
	0    -1   -1   0   
$EndComp
$Comp
L Turbidity_Sensor-rescue:R-US_R0805-Turbidity_Sensor-eagle-import R5
U 1 1 24F7EBBF
P 5800 4200
F 0 "R5" H 5650 4259 59  0000 L BNN
F 1 "3k3" H 5650 4070 59  0000 L BNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5800 4200 50  0001 C CNN
F 3 "" H 5800 4200 50  0001 C CNN
	1    5800 4200
	1    0    0    -1  
$EndComp
$Comp
L Turbidity_Sensor-rescue:C-USC0805-Turbidity_Sensor-eagle-import C1
U 1 1 E6F55D96
P 5200 4400
F 0 "C1" H 5240 4425 59  0000 L BNN
F 1 "100nF" H 5240 4235 59  0000 L BNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5200 4400 50  0001 C CNN
F 3 "" H 5200 4400 50  0001 C CNN
	1    5200 4400
	1    0    0    -1  
$EndComp
$Comp
L Turbidity_Sensor-rescue:C-USC0805-Turbidity_Sensor-eagle-import C2
U 1 1 03B11DE5
P 5200 3500
F 0 "C2" H 5240 3525 59  0000 L BNN
F 1 "10uF" H 5240 3335 59  0000 L BNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5200 3500 50  0001 C CNN
F 3 "" H 5200 3500 50  0001 C CNN
	1    5200 3500
	1    0    0    -1  
$EndComp
$Comp
L Turbidity_Sensor-rescue:GND-Turbidity_Sensor-eagle-import #SUPPLY01
U 1 1 9BB82FD5
P 5200 4800
F 0 "#SUPPLY01" H 5200 4800 50  0001 C CNN
F 1 "GND" H 5125 4675 59  0000 L BNN
F 2 "" H 5200 4800 50  0001 C CNN
F 3 "" H 5200 4800 50  0001 C CNN
	1    5200 4800
	1    0    0    -1  
$EndComp
$Comp
L Turbidity_Sensor-rescue:GND-Turbidity_Sensor-eagle-import #SUPPLY02
U 1 1 7668A807
P 8100 4900
F 0 "#SUPPLY02" H 8100 4900 50  0001 C CNN
F 1 "GND" H 8025 4775 59  0000 L BNN
F 2 "" H 8100 4900 50  0001 C CNN
F 3 "" H 8100 4900 50  0001 C CNN
	1    8100 4900
	1    0    0    -1  
$EndComp
$Comp
L Turbidity_Sensor-rescue:GND-Turbidity_Sensor-eagle-import #SUPPLY03
U 1 1 9BCD8B80
P 6950 4700
F 0 "#SUPPLY03" H 6950 4700 50  0001 C CNN
F 1 "GND" H 6875 4575 59  0000 L BNN
F 2 "" H 6950 4700 50  0001 C CNN
F 3 "" H 6950 4700 50  0001 C CNN
	1    6950 4700
	1    0    0    -1  
$EndComp
$Comp
L Turbidity_Sensor-rescue:GND-Turbidity_Sensor-eagle-import #SUPPLY04
U 1 1 0A61E534
P 2700 4300
F 0 "#SUPPLY04" H 2700 4300 50  0001 C CNN
F 1 "GND" H 2625 4175 59  0000 L BNN
F 2 "" H 2700 4300 50  0001 C CNN
F 3 "" H 2700 4300 50  0001 C CNN
	1    2700 4300
	1    0    0    -1  
$EndComp
$Comp
L Turbidity_Sensor-rescue:GND-Turbidity_Sensor-eagle-import #SUPPLY05
U 1 1 64E24A77
P 5200 3900
F 0 "#SUPPLY05" H 5200 3900 50  0001 C CNN
F 1 "GND" H 5125 3775 59  0000 L BNN
F 2 "" H 5200 3900 50  0001 C CNN
F 3 "" H 5200 3900 50  0001 C CNN
	1    5200 3900
	1    0    0    -1  
$EndComp
$Comp
L Turbidity_Sensor-rescue:+5V-Turbidity_Sensor-eagle-import #SUPPLY06
U 1 1 137BDED3
P 4700 3300
F 0 "#SUPPLY06" H 4700 3300 50  0001 C CNN
F 1 "+5V" H 4625 3425 59  0000 L BNN
F 2 "" H 4700 3300 50  0001 C CNN
F 3 "" H 4700 3300 50  0001 C CNN
	1    4700 3300
	1    0    0    -1  
$EndComp
$Comp
L Turbidity_Sensor-rescue:+5V-Turbidity_Sensor-eagle-import #SUPPLY07
U 1 1 B162DFB5
P 6950 3700
F 0 "#SUPPLY07" H 6950 3700 50  0001 C CNN
F 1 "+5V" H 6875 3825 59  0000 L BNN
F 2 "" H 6950 3700 50  0001 C CNN
F 3 "" H 6950 3700 50  0001 C CNN
	1    6950 3700
	1    0    0    -1  
$EndComp
$Comp
L Turbidity_Sensor-rescue:+5V-Turbidity_Sensor-eagle-import #SUPPLY08
U 1 1 F137449E
P 8300 3700
F 0 "#SUPPLY08" H 8300 3700 50  0001 C CNN
F 1 "+5V" H 8225 3825 59  0000 L BNN
F 2 "" H 8300 3700 50  0001 C CNN
F 3 "" H 8300 3700 50  0001 C CNN
	1    8300 3700
	1    0    0    -1  
$EndComp
$Comp
L Turbidity_Sensor-rescue:PINHD-1X55X2MM-Turbidity_Sensor-eagle-import JP1
U 1 1 A75A2B47
P 3000 4300
F 0 "JP1" H 2750 4625 59  0000 L BNN
F 1 "PINHD-1X55X2MM" H 2750 3900 59  0000 L BNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x05_P2.00mm_Vertical" H 3000 4300 50  0001 C CNN
F 3 "" H 3000 4300 50  0001 C CNN
	1    3000 4300
	-1   0    0    -1  
$EndComp
$Comp
L Turbidity_Sensor-rescue:PINHD-1X55X2MM-Turbidity_Sensor-eagle-import JP2
U 1 1 93E45719
P 4800 4300
F 0 "JP2" H 4550 4625 59  0000 L BNN
F 1 "PINHD-1X55X2MM" H 4550 3900 59  0000 L BNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x05_P2.00mm_Vertical" H 4800 4300 50  0001 C CNN
F 3 "" H 4800 4300 50  0001 C CNN
	1    4800 4300
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J1
U 1 1 5EF97311
P 8600 4100
F 0 "J1" H 8572 4032 50  0000 R CNN
F 1 "Conn_01x03_Male" H 8572 4123 50  0000 R CNN
F 2 "Connector_JST:JST_XH_S3B-XH-A_1x03_P2.50mm_Horizontal" H 8600 4100 50  0001 C CNN
F 3 "~" H 8600 4100 50  0001 C CNN
	1    8600 4100
	-1   0    0    1   
$EndComp
$Comp
L Amplifier_Operational:LMV321 U1
U 1 1 5EF9E4A1
P 6850 4200
F 0 "U1" H 6850 4681 50  0000 C CNN
F 1 "LMV321" H 6850 4590 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5_HandSoldering" H 6850 4200 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/lmv324.pdf" H 6850 4200 50  0001 C CNN
	1    6850 4200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6950 3800 6950 3900
Wire Wire Line
	6000 4200 6200 4200
Wire Wire Line
	6200 4200 6200 5000
Wire Wire Line
	6200 5000 7250 5000
Wire Wire Line
	7250 5000 7250 4300
Wire Wire Line
	7250 4300 7150 4300
Connection ~ 6200 4200
Wire Wire Line
	6200 4200 6550 4200
Wire Wire Line
	7300 4100 7150 4100
Wire Wire Line
	8300 4000 8400 4000
Wire Wire Line
	6950 4500 6950 4600
Text Label 7050 5000 0    50   ~ 0
V-
Text Label 7200 4100 0    50   ~ 0
V+
Connection ~ 7700 4100
$EndSCHEMATC
