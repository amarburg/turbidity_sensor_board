EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5200 3700 5200 3800
Text Label 5200 3700 0    10   ~ 0
GND
Wire Wire Line
	7700 4700 7700 4800
Text Label 7700 4700 0    10   ~ 0
GND
Text Label 8400 4000 0    10   ~ 0
+5V
Wire Wire Line
	4400 4100 4700 4100
Wire Wire Line
	4700 4100 4700 3400
Wire Wire Line
	4700 3400 5200 3400
Wire Wire Line
	5200 4300 5200 4200
Connection ~ 5200 4200
Wire Wire Line
	4700 4300 4400 4300
Wire Wire Line
	4400 4400 4700 4400
$Comp
L Turbidity_Sensor-rescue:ADAFRUIT_TRINKET-Turbidity_Sensor-eagle-import-Turbidity_Sensor_Parts-Turbidity_Sensor-rescue U$1
U 1 1 4D826FCC
P 3600 4000
F 0 "U$1" H 3600 4000 59  0000 L BNN
F 1 "ADAFRUIT_TRINKET" H 3600 4000 50  0001 C CNN
F 2 "Turbidity_Sensor:ADAFRUIT_TRINKET" H 3600 4000 50  0001 C CNN
F 3 "" H 3600 4000 50  0001 C CNN
	1    3600 4000
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LMV321 U1
U 1 1 5EF9E4A1
P 6850 4200
F 0 "U1" H 7050 4550 50  0000 C CNN
F 1 "LMV321" H 6950 4450 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5_HandSoldering" H 6850 4200 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/lmv324.pdf" H 6850 4200 50  0001 C CNN
	1    6850 4200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6200 4200 6200 5000
Wire Wire Line
	6200 5000 7250 5000
Wire Wire Line
	7250 5000 7250 4300
Wire Wire Line
	7250 4300 7150 4300
Connection ~ 6200 4200
Wire Wire Line
	6200 4200 6550 4200
Text Label 7050 5000 0    50   ~ 0
V-
Text Label 7200 4100 0    50   ~ 0
V+
$Comp
L Device:C C1
U 1 1 602789D9
P 5200 3550
F 0 "C1" H 5315 3596 50  0000 L CNN
F 1 "10uF" H 5315 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5238 3400 50  0001 C CNN
F 3 "~" H 5200 3550 50  0001 C CNN
	1    5200 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 60278D5A
P 5200 4450
F 0 "C2" H 5315 4496 50  0000 L CNN
F 1 "10uF" H 5315 4405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5238 4300 50  0001 C CNN
F 3 "~" H 5200 4450 50  0001 C CNN
	1    5200 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 60282DBD
P 7700 4800
F 0 "#PWR0102" H 7700 4550 50  0001 C CNN
F 1 "GND" H 7705 4627 50  0000 C CNN
F 2 "" H 7700 4800 50  0001 C CNN
F 3 "" H 7700 4800 50  0001 C CNN
	1    7700 4800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 60282E02
P 6950 4600
F 0 "#PWR0103" H 6950 4350 50  0001 C CNN
F 1 "GND" H 6955 4427 50  0000 C CNN
F 2 "" H 6950 4600 50  0001 C CNN
F 3 "" H 6950 4600 50  0001 C CNN
	1    6950 4600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 602830E3
P 5500 4700
F 0 "#PWR0104" H 5500 4450 50  0001 C CNN
F 1 "GND" H 5505 4527 50  0000 C CNN
F 2 "" H 5500 4700 50  0001 C CNN
F 3 "" H 5500 4700 50  0001 C CNN
	1    5500 4700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 6028333C
P 5200 3800
F 0 "#PWR0105" H 5200 3550 50  0001 C CNN
F 1 "GND" H 5205 3627 50  0000 C CNN
F 2 "" H 5200 3800 50  0001 C CNN
F 3 "" H 5200 3800 50  0001 C CNN
	1    5200 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 6028574D
P 7700 4550
F 0 "R4" H 7770 4596 50  0000 L CNN
F 1 "47k" H 7770 4505 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7630 4550 50  0001 C CNN
F 3 "~" H 7700 4550 50  0001 C CNN
	1    7700 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 602861C6
P 7500 4100
F 0 "R3" V 7293 4100 50  0000 C CNN
F 1 "10k" V 7384 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7430 4100 50  0001 C CNN
F 3 "~" H 7500 4100 50  0001 C CNN
	1    7500 4100
	0    1    1    0   
$EndComp
Wire Wire Line
	7650 4100 7700 4100
Connection ~ 7700 4100
Wire Wire Line
	7150 4100 7350 4100
Wire Wire Line
	7700 4100 7700 4400
Wire Wire Line
	5200 4200 5500 4200
$Comp
L Device:R R2
U 1 1 602886C9
P 5800 4200
F 0 "R2" V 5593 4200 50  0000 C CNN
F 1 "4k7 0.05%" V 5684 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5730 4200 50  0001 C CNN
F 3 "~" H 5800 4200 50  0001 C CNN
	1    5800 4200
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 60288BDA
P 5500 4400
F 0 "R1" H 5430 4354 50  0000 R CNN
F 1 "10k  0.05%" H 5430 4445 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5430 4400 50  0001 C CNN
F 3 "~" H 5500 4400 50  0001 C CNN
	1    5500 4400
	-1   0    0    1   
$EndComp
Wire Wire Line
	5500 4550 5500 4700
Wire Wire Line
	5500 4250 5500 4200
Connection ~ 5500 4200
Wire Wire Line
	5500 4200 5650 4200
Wire Wire Line
	5950 4200 6200 4200
$Comp
L Connector_Generic:Conn_01x06 J2
U 1 1 6028DFA0
P 9800 4200
F 0 "J2" H 9880 4192 50  0000 L CNN
F 1 "Conn_01x06" H 9880 4101 50  0000 L CNN
F 2 "Connector_JST:JST_PH_S6B-PH-K_1x06_P2.00mm_Horizontal" H 9800 4200 50  0001 C CNN
F 3 "~" H 9800 4200 50  0001 C CNN
	1    9800 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 6028E7BE
P 9200 3700
F 0 "#PWR0107" H 9200 3450 50  0001 C CNN
F 1 "GND" H 9205 3527 50  0000 C CNN
F 2 "" H 9200 3700 50  0001 C CNN
F 3 "" H 9200 3700 50  0001 C CNN
	1    9200 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 3700 9200 3600
Wire Wire Line
	9200 3600 9450 3600
Wire Wire Line
	7700 4100 9600 4100
Text Label 8650 4100 0    50   ~ 0
SEAPOINT_SIGNAL
Wire Wire Line
	9600 4200 9450 4200
Wire Wire Line
	9450 4200 9450 4000
Wire Wire Line
	9600 4000 9450 4000
Connection ~ 9450 4000
Wire Wire Line
	9450 4000 9450 3600
Text Notes 10400 4450 0    50   ~ 0
1. Power GND\n2. Signal Output (0-5V)\n3. Signal GND\n4. Power in (7-20VDC)\n5. Gain Control A\n6. Gain Control B
$Comp
L power:+10V #PWR0108
U 1 1 60295C69
P 7950 2600
F 0 "#PWR0108" H 7950 2450 50  0001 C CNN
F 1 "+10V" H 7965 2773 50  0000 C CNN
F 2 "" H 7950 2600 50  0001 C CNN
F 3 "" H 7950 2600 50  0001 C CNN
	1    7950 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J1
U 1 1 6029709E
P 1750 4300
F 0 "J1" H 1668 4617 50  0000 C CNN
F 1 "Conn_01x04" H 1668 4526 50  0000 C CNN
F 2 "Connector_JST:JST_PH_S4B-PH-K_1x04_P2.00mm_Horizontal" H 1750 4300 50  0001 C CNN
F 3 "~" H 1750 4300 50  0001 C CNN
	1    1750 4300
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 6029B3AE
P 2000 4600
F 0 "#PWR0109" H 2000 4350 50  0001 C CNN
F 1 "GND" H 2005 4427 50  0000 C CNN
F 2 "" H 2000 4600 50  0001 C CNN
F 3 "" H 2000 4600 50  0001 C CNN
	1    2000 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 4600 2000 4500
Wire Wire Line
	2000 4500 1950 4500
Wire Wire Line
	2000 4200 1950 4200
Text Label 2050 4400 0    50   ~ 0
UART_RX_TTL
Text Label 2050 4300 0    50   ~ 0
UART_TX_TTL
Wire Wire Line
	1950 4300 3400 4300
Wire Wire Line
	1950 4400 3400 4400
Wire Wire Line
	4400 4200 5200 4200
Wire Wire Line
	2000 4100 2000 4200
$Comp
L power:VBUS #PWR0110
U 1 1 602AB2BC
P 4700 3350
F 0 "#PWR0110" H 4700 3200 50  0001 C CNN
F 1 "VBUS" H 4715 3523 50  0000 C CNN
F 2 "" H 4700 3350 50  0001 C CNN
F 3 "" H 4700 3350 50  0001 C CNN
	1    4700 3350
	1    0    0    -1  
$EndComp
Connection ~ 4700 3400
$Comp
L power:VDD #PWR0111
U 1 1 602AB9EC
P 2000 3650
F 0 "#PWR0111" H 2000 3500 50  0001 C CNN
F 1 "VDD" H 2015 3823 50  0000 C CNN
F 2 "" H 2000 3650 50  0001 C CNN
F 3 "" H 2000 3650 50  0001 C CNN
	1    2000 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Schottky D2
U 1 1 602ABEC7
P 5600 2900
F 0 "D2" H 5600 2683 50  0000 C CNN
F 1 "D_Schottky" H 5600 2774 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323_HandSoldering" H 5600 2900 50  0001 C CNN
F 3 "~" H 5600 2900 50  0001 C CNN
	1    5600 2900
	-1   0    0    1   
$EndComp
$Comp
L Device:D_Schottky D1
U 1 1 602ACB7C
P 5600 2600
F 0 "D1" H 5600 2383 50  0000 C CNN
F 1 "D_Schottky" H 5600 2474 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323_HandSoldering" H 5600 2600 50  0001 C CNN
F 3 "~" H 5600 2600 50  0001 C CNN
	1    5600 2600
	-1   0    0    1   
$EndComp
Wire Wire Line
	2000 4100 3400 4100
Wire Wire Line
	2000 4100 2000 3650
Connection ~ 2000 4100
$Comp
L power:VDD #PWR0112
U 1 1 602AE6EF
P 5300 2550
F 0 "#PWR0112" H 5300 2400 50  0001 C CNN
F 1 "VDD" H 5315 2723 50  0000 C CNN
F 2 "" H 5300 2550 50  0001 C CNN
F 3 "" H 5300 2550 50  0001 C CNN
	1    5300 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 2550 5300 2600
Wire Wire Line
	5300 2600 5450 2600
Wire Wire Line
	5200 3400 5200 2900
Wire Wire Line
	5200 2900 5450 2900
Connection ~ 5200 3400
Wire Wire Line
	5750 2900 5750 2600
$Comp
L Turbidity_Sensor-rescue:LM2665M6-Regulator_SwitchedCapacitor-Turbidity_Sensor-rescue U2
U 1 1 602B1CAC
P 6850 2700
F 0 "U2" H 6850 3067 50  0000 C CNN
F 1 "LM2665M6" H 6850 2976 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 6900 2350 50  0001 L CNN
F 3 "https://www.ti.com/lit/ds/symlink/lm2665.pdf" H 5000 3950 50  0001 C CNN
	1    6850 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 2600 6150 2600
Connection ~ 5750 2600
Connection ~ 6150 2600
Wire Wire Line
	6150 2600 6450 2600
Wire Wire Line
	6450 2800 6450 3000
Wire Wire Line
	6450 3000 6850 3000
$Comp
L power:GND #PWR0114
U 1 1 602B54F2
P 6850 3100
F 0 "#PWR0114" H 6850 2850 50  0001 C CNN
F 1 "GND" H 6855 2927 50  0000 C CNN
F 2 "" H 6850 3100 50  0001 C CNN
F 3 "" H 6850 3100 50  0001 C CNN
	1    6850 3100
	1    0    0    -1  
$EndComp
Connection ~ 6850 3000
$Comp
L Device:C C3
U 1 1 602B964B
P 6150 2750
F 0 "C3" H 6265 2796 50  0000 L CNN
F 1 "3.3 uF" H 6265 2705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6188 2600 50  0001 C CNN
F 3 "~" H 6150 2750 50  0001 C CNN
	1    6150 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 602B9D81
P 7500 2850
F 0 "C4" H 7615 2896 50  0000 L CNN
F 1 "3.3 uF" H 7615 2805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7538 2700 50  0001 C CNN
F 3 "~" H 7500 2850 50  0001 C CNN
	1    7500 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 602BA192
P 7950 2850
F 0 "C5" H 8065 2896 50  0000 L CNN
F 1 "3.3 uF" H 8065 2805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7988 2700 50  0001 C CNN
F 3 "~" H 7950 2850 50  0001 C CNN
	1    7950 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 2900 6150 3000
Wire Wire Line
	6150 3000 6450 3000
Connection ~ 6450 3000
Wire Wire Line
	7250 2700 7500 2700
Wire Wire Line
	7500 3000 7250 3000
Wire Wire Line
	7250 3000 7250 2800
$Comp
L power:GND #PWR0115
U 1 1 602BD9E4
P 7950 3000
F 0 "#PWR0115" H 7950 2750 50  0001 C CNN
F 1 "GND" H 7955 2827 50  0000 C CNN
F 2 "" H 7950 3000 50  0001 C CNN
F 3 "" H 7950 3000 50  0001 C CNN
	1    7950 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 2600 7300 2600
Wire Wire Line
	7950 2600 7950 2700
Connection ~ 7950 2600
Wire Wire Line
	7950 2600 8500 2600
Wire Wire Line
	8500 2600 8500 4300
Wire Wire Line
	8500 4300 9600 4300
$Comp
L Device:D D3
U 1 1 602C10C7
P 6850 2200
F 0 "D3" H 6850 1983 50  0000 C CNN
F 1 "D" H 6850 2074 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323_HandSoldering" H 6850 2200 50  0001 C CNN
F 3 "~" H 6850 2200 50  0001 C CNN
	1    6850 2200
	-1   0    0    1   
$EndComp
Wire Wire Line
	6450 2600 6450 2200
Wire Wire Line
	6450 2200 6700 2200
Connection ~ 6450 2600
Wire Wire Line
	7000 2200 7300 2200
Wire Wire Line
	7300 2200 7300 2600
Connection ~ 7300 2600
Wire Wire Line
	7300 2600 7950 2600
$Comp
L Transistor_FET:BS170F Q2
U 1 1 602C4DFD
P 9300 5700
F 0 "Q2" H 9505 5746 50  0000 L CNN
F 1 "BS170F" H 9505 5655 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 9500 5625 50  0001 L CIN
F 3 "http://www.diodes.com/assets/Datasheets/BS170F.pdf" H 9300 5700 50  0001 L CNN
	1    9300 5700
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:BS170F Q1
U 1 1 602CEA90
P 8850 4900
F 0 "Q1" H 9055 4946 50  0000 L CNN
F 1 "BS170F" H 9055 4855 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 9050 4825 50  0001 L CIN
F 3 "http://www.diodes.com/assets/Datasheets/BS170F.pdf" H 8850 4900 50  0001 L CNN
	1    8850 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 602D1C04
P 8950 5150
F 0 "#PWR0116" H 8950 4900 50  0001 C CNN
F 1 "GND" H 8955 4977 50  0000 C CNN
F 2 "" H 8950 5150 50  0001 C CNN
F 3 "" H 8950 5150 50  0001 C CNN
	1    8950 5150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 602D5516
P 8400 4900
F 0 "R5" V 8607 4900 50  0000 C CNN
F 1 "1k" V 8516 4900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8330 4900 50  0001 C CNN
F 3 "~" H 8400 4900 50  0001 C CNN
	1    8400 4900
	0    1    -1   0   
$EndComp
Wire Wire Line
	8650 4900 8550 4900
Wire Wire Line
	8550 4900 8550 5150
Wire Wire Line
	8850 5150 8950 5150
Wire Wire Line
	8950 5150 8950 5100
Connection ~ 8950 5150
Wire Wire Line
	8950 4700 8950 4400
Wire Wire Line
	8950 4400 9600 4400
Connection ~ 8550 4900
$Comp
L Device:R R6
U 1 1 602DD76D
P 9250 5950
F 0 "R6" V 9043 5950 50  0000 C CNN
F 1 "10k" V 9134 5950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9180 5950 50  0001 C CNN
F 3 "~" H 9250 5950 50  0001 C CNN
	1    9250 5950
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 602DE1B4
P 9400 5950
F 0 "#PWR0117" H 9400 5700 50  0001 C CNN
F 1 "GND" H 9405 5777 50  0000 C CNN
F 2 "" H 9400 5950 50  0001 C CNN
F 3 "" H 9400 5950 50  0001 C CNN
	1    9400 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 5950 9400 5900
Connection ~ 9400 5950
Wire Wire Line
	9400 4500 9400 5500
Wire Wire Line
	9100 5700 9100 5950
Wire Wire Line
	9100 5700 9050 5700
Connection ~ 9100 5700
Text Label 8450 5700 0    50   ~ 0
G_GAINB
Text Label 7900 4900 0    50   ~ 0
G_GAINA
Text Label 4550 4300 0    50   ~ 0
G_GAINA
Text Label 4550 4400 0    50   ~ 0
G_GAINB
Wire Wire Line
	9400 4500 9600 4500
$Comp
L power:+10V #PWR0101
U 1 1 602F2FC3
P 6950 3850
F 0 "#PWR0101" H 6950 3700 50  0001 C CNN
F 1 "+10V" H 6965 4023 50  0000 C CNN
F 2 "" H 6950 3850 50  0001 C CNN
F 3 "" H 6950 3850 50  0001 C CNN
	1    6950 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 602FB43D
P 8700 5150
F 0 "R7" V 8907 5150 50  0000 C CNN
F 1 "10k" V 8816 5150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8630 5150 50  0001 C CNN
F 3 "~" H 8700 5150 50  0001 C CNN
	1    8700 5150
	0    1    -1   0   
$EndComp
Wire Wire Line
	7900 4900 8250 4900
$Comp
L Device:R R8
U 1 1 60302339
P 8900 5700
F 0 "R8" V 8693 5700 50  0000 C CNN
F 1 "1k" V 8784 5700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8830 5700 50  0001 C CNN
F 3 "~" H 8900 5700 50  0001 C CNN
	1    8900 5700
	0    1    1    0   
$EndComp
Wire Wire Line
	8750 5700 8450 5700
$Comp
L power:GND #PWR0106
U 1 1 6034CB09
P 3300 4200
F 0 "#PWR0106" H 3300 3950 50  0001 C CNN
F 1 "GND" V 3305 4072 50  0000 R CNN
F 2 "" H 3300 4200 50  0001 C CNN
F 3 "" H 3300 4200 50  0001 C CNN
	1    3300 4200
	0    1    1    0   
$EndComp
NoConn ~ 4400 4500
NoConn ~ 3400 4500
Wire Wire Line
	6950 3850 6950 3900
Wire Wire Line
	6950 4600 6950 4500
Wire Wire Line
	4700 3400 4700 3350
Wire Wire Line
	5200 4600 5200 4700
Wire Wire Line
	5200 4700 5500 4700
Connection ~ 5500 4700
Wire Wire Line
	6850 3000 6850 3100
Wire Wire Line
	3300 4200 3400 4200
Text Label 5300 2600 0    50   ~ 0
VDD
Text Label 4800 3400 0    50   ~ 0
VBUS
$Comp
L Device:C C6
U 1 1 6037A9F3
P 2300 3800
F 0 "C6" H 2415 3846 50  0000 L CNN
F 1 "10uF" H 2415 3755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2338 3650 50  0001 C CNN
F 3 "~" H 2300 3800 50  0001 C CNN
	1    2300 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 3650 2000 3650
Connection ~ 2000 3650
$Comp
L power:GND #PWR0113
U 1 1 6037EBEB
P 2300 3950
F 0 "#PWR0113" H 2300 3700 50  0001 C CNN
F 1 "GND" H 2305 3777 50  0000 C CNN
F 2 "" H 2300 3950 50  0001 C CNN
F 3 "" H 2300 3950 50  0001 C CNN
	1    2300 3950
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 60382C83
P 2500 6350
F 0 "H1" H 2600 6396 50  0000 L CNN
F 1 "MountingHole" H 2600 6305 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 2500 6350 50  0001 C CNN
F 3 "~" H 2500 6350 50  0001 C CNN
	1    2500 6350
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 60382EA9
P 2500 6550
F 0 "H2" H 2600 6596 50  0000 L CNN
F 1 "MountingHole" H 2600 6505 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 2500 6550 50  0001 C CNN
F 3 "~" H 2500 6550 50  0001 C CNN
	1    2500 6550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 60382FD0
P 2500 6750
F 0 "H3" H 2600 6796 50  0000 L CNN
F 1 "MountingHole" H 2600 6705 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 2500 6750 50  0001 C CNN
F 3 "~" H 2500 6750 50  0001 C CNN
	1    2500 6750
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 6038325C
P 2500 6950
F 0 "H4" H 2600 6996 50  0000 L CNN
F 1 "MountingHole" H 2600 6905 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 2500 6950 50  0001 C CNN
F 3 "~" H 2500 6950 50  0001 C CNN
	1    2500 6950
	1    0    0    -1  
$EndComp
$EndSCHEMATC
